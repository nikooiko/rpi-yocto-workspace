# Raspberry PI + Yocto Workspace 

## Prerequisites
- Docker

## Instructions

Steps to build firmware:
- `source ./setup-environment`
- `build-interractive` (host env)
- `build` (builder env)

NOTE: `build-help` to see all available build commands (supports host and builder)

## Project Structure
- **builder**: The docker-based builder files
- **scripts**: Contains all the host scripts that are executed at the host environment.

## Related Repositories
- https://gitlab.com/nikooiko/rpi-yocto-manifests
