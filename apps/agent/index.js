'use strict';

const {createBluetooth} = require('node-ble');

async function app() {
  const {bluetooth, destroy} = createBluetooth();
  try {
    const adapter = await bluetooth.defaultAdapter();
    console.log('app init');
    if (!await adapter.isDiscovering()) await adapter.startDiscovery();
    console.log('discovering');
    while (true) {
      const device = await adapter.waitDevice('00:00:00:00:00:00');
      console.log('device discovered', await device.getName(), await device.getAddress());
      await device.connect();
      console.log('device connected');
      const gattServer = await device.gatt();
      const service = await gattServer.getPrimaryService('uuid');
      const characteristic = await service.getCharacteristic('uuid');
      await characteristic.startNotifications();
      characteristic.on('valuechanged', (data) => console.log(data));
      console.log('waiting for data');
      await new Promise(resolve => setTimeout(resolve, 60000));
      device.disconnect();
    }
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
}

if (require.main === module) {
  app();
}

