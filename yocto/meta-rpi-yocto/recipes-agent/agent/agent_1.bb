SUMMARY = "Adds the agent application to the system"
DESCRIPTION = "Installs the agent application in the image and creates a daemon."
AUTHOR = "RPI Yocto Workspace awesome author"
LICENSE = "CLOSED"

S = "${WORKDIR}"
FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"
SRC_URI = "file://opt/ file://etc/"
FILES_${PN} += " /opt /opt/agent /opt/global-modules /etc /etc/init.d /etc/init.d/pm2-init"
RDEPENDS_${PN} = "bash perl python3 nodejs nodejs-npm"

# Skip checks below to avoid QA errors. Comment out to see what are the currently skipped errors.
# This is done because we have already checked the dependencies and currently, the remaining errors
# do not affect the operation of the firmware. Arch checks are specificaly disabled because this
# recipe expects that all extra files are either architecture agnostic or are built for the target
# architecture.
INSANE_SKIP_${PN} = "file-rdeps staticdev already-stripped arch"

# Because we are unable to split and strip debug symbols from files (unrecognized file format) we
# must skip them.
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"

do_install() {
    # make sure target directory exist
    install -d ${D}${sysconfdir}/init.d/
    install -d ${D}${sysconfdir}/rc5.d/
    install -d ${D}${sysconfdir}/rc6.d/
    install -d ${D}/opt

    # install agent and global modules
    cp -r opt/{agent,global-modules} ${D}/opt/

    # install etc files
    install -m 0755 etc/init.d/pm2-init ${D}${sysconfdir}/init.d/
    install -d etc/dbus-1 ${D}${sysconfdir}/
    ln -sf /etc/init.d/pm2-init ${D}${sysconfdir}/rc5.d/S99pm2-init
    ln -sf /etc/init.d/pm2-init ${D}${sysconfdir}/rc6.d/K20pm2-init
}