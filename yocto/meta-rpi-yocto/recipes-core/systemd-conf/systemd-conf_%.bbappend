FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"

SRC_URI_append = " \
    file://etc/systemd/network/10-eth.network \
    file://etc/systemd/network/11-wlan.network \
"

FILES_${PN} += " \
    ${sysconfdir}/systemd/network/10-eth.network \
    ${sysconfdir}/systemd/network/11-wlan.network \
"

do_install_append() {
    install -d ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/etc/systemd/network/10-eth.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/etc/systemd/network/11-wlan.network ${D}${sysconfdir}/systemd/network
}

