FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"

SRC_URI_append = " file://etc/profile"

FILES_${PN} += " \
    ${sysconfdir}/profile \
"

do_install_append() {
    # start with a newline to avoid conflicts
    echo "" >> ${D}${sysconfdir}/profile

    cat ${sysconfdir}/profile >> ${D}${sysconfdir}/profile

    # end with a newline to avoid conflicts
    echo "" >> ${D}${sysconfdir}/profile
}

