SUMMARY = "RPI Yocto image"

LICENSE = "MIT"

IMAGE_FEATURES += " ssh-server-dropbear"

IMAGE_FSTYPES = "rpi-sdimg ext4.gz"

inherit core-image

IMAGE_INSTALL += " \
    kernel-modules \
    base-files \
    base-passwd \
    busybox \
    mtd-utils \
	mtd-utils-ubifs \
    libconfig \
    swupdate \
    swupdate-www \
    util-linux-sfdisk \
    systemd \
    raspi-gpio \
    bluez5 pi-bluetooth \
    minicom \
    socat \
    vim \
    curl \
    agent \
    "
