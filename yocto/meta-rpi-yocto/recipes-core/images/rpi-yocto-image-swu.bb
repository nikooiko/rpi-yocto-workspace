DESCRIPTION = "RPI Yocto SWU image"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit swupdate

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI = " \
    file://sw-description \
"

# images to build before building swupdate image
IMAGE_DEPENDS = "rpi-yocto-image"

# images and files that will be included in the .swu image
SWUPDATE_IMAGES = "rpi-yocto-image uImage"

SWUPDATE_IMAGES_FSTYPES[rpi-yocto-image] = ".ext4.gz"
SWUPDATE_IMAGES_FSTYPES[uImage] = ".bin"