DESCRIPTION = "RPI Yocto dev SWU image"

require rpi-yocto-image-swu.bb

# images to build before building swupdate image
IMAGE_DEPENDS = "rpi-yocto-dev-image"

# images and files that will be included in the .swu image
SWUPDATE_IMAGES = "rpi-yocto-dev-image uImage"

SWUPDATE_IMAGES_FSTYPES[rpi-yocto-dev-image] = ".ext4.gz"
SWUPDATE_IMAGES_FSTYPES[uImage] = ".bin"