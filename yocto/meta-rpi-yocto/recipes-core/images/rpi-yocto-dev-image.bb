SUMMARY = "RPI Yocto development image"

inherit core-image
require rpi-yocto-image.bb

IMAGE_FEATURES += " tools-sdk tools-debug debug-tweaks"

IMAGE_INSTALL += "tcpdump"
