FROM crops/poky

USER root

# Install Prerequisites (+ vim for interractive editing)
RUN apt-get -y update && apt-get -y install  \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    jq \
    iptables \
    vim \
    && curl -o /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo && chmod a+x /usr/local/bin/repo \
    && curl -fsSL https://deb.nodesource.com/setup_12.x | sudo -E bash - && apt-get install -y nodejs

# Install RPI dependencies
ARG RELEASE
ARG RPI_YOCTO_SRC=/usr/local/rpi-yocto
RUN install -o 1000 -g 1000 -d ${RPI_YOCTO_SRC}
WORKDIR ${RPI_YOCTO_SRC}
RUN repo init -u https://gitlab.com/nikooiko/rpi-yocto-manifests \
    -b ${RELEASE} \
    -m default.xml \
    && repo sync
ENV RPI_YOCTO_SRC=${RPI_YOCTO_SRC}

# Cleanup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create pokyuser
RUN ["/usr/bin/distro-entry.sh", "/usr/bin/dumb-init", "--", "/usr/bin/poky-entry.py"]

# Install project's layer
COPY yocto/meta-rpi-yocto ${RPI_YOCTO_SRC}/layers/meta-rpi-yocto
# Install agent inside project's layer
COPY apps/agent ${RPI_YOCTO_SRC}/layers/meta-rpi-yocto/recipes-agent/agent/agent/opt/agent
RUN ${RPI_YOCTO_SRC}/layers/meta-rpi-yocto/recipes-agent/agent/agent/opt/agent/scripts/install-agent-modules --prefix ${RPI_YOCTO_SRC}/layers/meta-rpi-yocto/recipes-agent/agent/agent/opt/agent
RUN ${RPI_YOCTO_SRC}/layers/meta-rpi-yocto/recipes-agent/agent/agent/opt/agent/scripts/install-global-modules --prefix ${RPI_YOCTO_SRC}/layers/meta-rpi-yocto/recipes-agent/agent/agent/opt/global-modules

# Install builder scripts
COPY builder/scripts/* /usr/local/bin/

# Prepare for work directory
WORKDIR /workdir
RUN chown -R pokyuser:pokyuser /workdir
USER pokyuser
# Set Builder's paths. For now workdir is used for everything
ENV YOCTO_BUILD_DIR=/workdir/build/yocto-build \
    DL_DIR=/workdir/build/downloads \
    SSTATE_DIR=/workdir/build/sstate-cache

ENTRYPOINT ["entrypoint"]